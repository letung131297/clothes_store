<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\Category;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        $category_sanpham = Category::where('parent_id', '=', '1')->get();
        $category_tintuc = Category::where('parent_id', '=', '2')->get();

        $category_menu = [$category_sanpham, $category_tintuc];
        View::share('category_menu', $category_menu);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
