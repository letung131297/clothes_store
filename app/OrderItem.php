<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class OrderItem extends Model
{
    public $table = 'order_items';
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'product_id', 'order_id', 'quantity', 'price', 'created_at', 'updated_at'
    ];


    public function getAll($id) 
    {
    	return DB::table('order_items')
    			->where('order_id', $id)
    			->leftjoin('products', 'order_items.product_id', '=', 'products.id')
    			->select('order_items.*', 'products.name')
    			->get();
    }
}
