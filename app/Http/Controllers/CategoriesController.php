<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Carbon\Carbon;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();

        return view('admin.category', compact('categories', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //return view('admin.category.createCategory');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cat = new Category();

        $cat->name = $request->name;
        $cat->parent_id = $request->parent_id;
        $cat->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categories = Category::all();
        $category = Category::FindOrFail($id);
        if ($category != false) {
            return view('admin.showCategory', ['category' => $category, 'categories' => $categories]);
        } else {
            return Redirect::back()->with('msg', 'Danh mục không tồn tại !');;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /*$category = Category::FindOrFail($id);
        if ($cat != false) {
            return view('admin.category.editCategory', compact('category'));
        } else {
            return Redirect::back()->with('msg', 'Danh mục không tồn tại !');;
        }*/
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        unset($data['_token']);
        Category::where('id', $id)
                ->update($data);
        return redirect('admin/categories')->with('success', 'Sửa thành công');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Category::remove($id);
    }


    public function insert(Request $request) 
    {
        $cat_exist = Category::where('name', $request->name)->get();

        if (count($cat_exist) != 0) {

            return redirect('admin/categories')->with('error', 'Danh mục đã tồn tại');
            
        } else {

            $cat = new Category();

            $data = $request->all();

            $cat->create($data);

            return redirect('admin/categories')->with('success', 'Thêm thành công');
        }

        

        
    }

    public function delete($id)
    {
        $cat = new Category();
        $cat = Category::find($id);
        $cat->delete();
        return redirect('admin/categories')->with('success', 'Xóa thành công');
    }


}
