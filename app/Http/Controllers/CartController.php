<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class CartController extends Controller
{

    public function showCart() 
    {

    	$products = [];
    	$totalMoney = 0;
    	if (isset($_SESSION['cart'])) {
    		foreach ($_SESSION['cart'] as $product_id => $quantity) {
	    		$product = Product::find($product_id)->toArray();
	    		$product['cart_quantity'] = $quantity;
	    		$products[] = $product;
	    	
	    		$totalMoney += $product['price'] * ( 1- $product['sale']/100) * $quantity;
	    	}
    	}
    	

        return view('client.cart', ['products' => $products, 'totalMoney' => $totalMoney ]);
    }


    public function addToCart(Request $request)
    {

    	if(isset($_SESSION['cart'][$request->product_id])) {

			$_SESSION['cart'][$request->product_id] = $request->quantity + $_SESSION['cart'][$request->product_id];
		} else {

			$_SESSION['cart'][$request->product_id] = $request->quantity;

		}

		return redirect('cart');

    }

    public function updateCart(Request $request)
    {
    	$data_request = $request->all()['cart'];
    	foreach ($data_request as $product_id => $quantity) {
            if($quantity == 0) {
                unset($_SESSION['cart'][ $product_id ]);
            } else {
                if(isset($_SESSION['cart'][$product_id])) {

                    $_SESSION['cart'][$product_id] = $quantity;
                } else {

                    $_SESSION['cart'][$product_id] = $quantity;

                }
            }

    	}

		return redirect('cart')->with('success', 'Cập nhật giỏ hàng thành công');
    }

    public function emptyCart() 
    {
    	unset($_SESSION['cart']);

    	return redirect('cart');
    }


}
