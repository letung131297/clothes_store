<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    public $table = 'products';
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'category_id', 'image', 'price', 'quantity', 'description', 'short_description', 'sale', 'sex', 'view', 'created_at', 'updated_at'
    ];

    public function getProductByKeyWord($s) 
    {
    	return DB::table('products')
    			->where('name','like','%'.$s.'%')
    			->orWhere('description', 'like', '%'.$s.'%')
    			->orWhere('short_description', 'like', '%'.$s.'%')
    			->get();
    }

    public function productOrder($orderBy, $order)
    {
    	return DB::table('products')
    			->orderBy($orderBy, $order)
    			->get();
    }
}
