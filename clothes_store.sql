-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 30, 2018 at 04:43 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.1.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `clothes_store`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 'Sản phẩm', 0, '2018-09-29 04:04:48', NULL),
(2, 'Tin tức', 0, '2018-09-29 04:04:48', NULL),
(3, 'Quần Áo Nam', 1, '2018-09-29 04:07:49', '2018-09-29 04:07:49'),
(4, 'Phụ Kiện Nam', 1, '2018-09-29 04:08:24', '2018-09-29 04:08:24'),
(5, 'Giày Nam', 1, '2018-09-29 04:08:39', '2018-09-29 04:08:39'),
(7, 'Xu Hướng Thời Trang', 2, '2018-09-29 05:32:47', '2018-09-29 05:32:47'),
(8, 'Thời trang Thu', 2, '2018-09-29 05:32:58', '2018-09-29 05:32:58'),
(9, 'Thời trang Hè', 2, '2018-09-29 05:33:08', '2018-09-29 05:33:08');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_08_12_000000_create_users_table', 1),
(2, '2018_08_12_100000_create_password_resets_table', 1),
(3, '2018_09_04_000001_create_categories_table', 1),
(4, '2018_09_04_000002_create_products_table', 1),
(5, '2018_09_04_000003_create_orders', 1),
(6, '2018_09_04_000004_create_order_items', 1),
(7, '2018_09_18_103412_create_posts', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `name`, `address`, `description`, `phone`, `total`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 'Nam', 'Hà Nội', 'Khách quen', '09123823821', '410250', '0', '2018-09-29 05:50:10', '2018-09-29 05:50:10');

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `product_id`, `order_id`, `quantity`, `price`, `created_at`, `updated_at`) VALUES
(1, 3, 1, 1, '320000', '2018-09-29 05:50:10', '2018-09-29 05:50:10'),
(2, 11, 1, 1, '90250', '2018-09-29 05:50:10', '2018-09-29 05:50:10');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `category_id`, `title`, `short_content`, `content`, `created_at`, `updated_at`) VALUES
(1, 8, 'Sơ mi công sở đẹp thời trang cho nàng diện thu', 'Những mẫu sơ mi thu đông năm nay của thời trang LOZA đang cực hót với chị em công sở, được thiết kế nhiều mẫu trẻ trung với đủ sắc màu mới mẻ sẽ là món đồ không thể bỏ qua trong các set đồ đến văn phòng những ngày thu này nhé.', 'Được ra mắt theo các BST từ tháng 8, thời trang LOZA sẽ là tiêu điểm lựa chọn cho các cô nàng mùa thu 2018 này, dưới đây là các siêu phẩm đặc biệt nhất được tuyển chọn, cùng ngắm xem nhé!\r\n+Chiếc sơ mi phong cách thời trang hàn quốc cực xinh với kiểu thiết kế nơ liền điệu, gam màu cam nổi bật khi chọn kết hợp cùng chân váy bút chì đen.\r\n+Những mẫu sơ mi công sở mùa thu đẹp và tiện dụng hơn với các thiết kế dài tay hoặc dáng tay lỡ, vẫn tư tin để mix cùng chân váy, nhưng thanh lịch hợp phong cách thu thì các nàng không thể bỏ qua set đồ với quần tây.\r\n+Lạ mắt với một chiếc sơ mi tay lỡ xòe điệu được kết hợp họa tiết màu tối. Kiểu cổ chun thắt nơ kín hợp những ngày se lạnh. Với họa tiết màu trầm này sẽ làm nền cho nàng mix cùng chân váy bút chì đỏ nổi bật cực đẹp.\r\n+Sơ mi trắng cổ đức đẹp thanh lịch chưa bao giờ là lỗi mốt với thiết kế tay dài được kết hợp cùng quần tây đen chuẩn công sở. Dáng sơ mi trắng cổ đức này còn là gu để thu đông các cô nàng kết hợp đẹp cùng áo vest mặc ngoài.\r\n+Một chiếc sơ mi nữ cổ đức trở nên nữ tính và điệu hơn nhiều khi được tạo viền cánh sen nhỏ dọc khuy cúc.\r\n+Những chiếc sơ mi thu dáng tay dài đẹp hơn, nữ tính hơn với kiểu thiết kế cổ thắt nơ duyên dáng.\r\n+Họa tiết phối màu đẹp xinh cho nàng với thiết kế tay lỡ, gam nền trắng nổi bật khi được chọn kết hợp với chân váy bút chì đỏ.\r\nNhững mẫu sơ mi tại thời trang LOZA không chỉ được thiết kế kiểu dáng mới lạ mà chất liệu cực đẹp, nhẹ nhàng luôn tạo cảm giác thoải mái cho người mặc, đặc biệt với chính sách bán hàng tốt nhất các quý cô thả ga mua sắm với giá cực ưu đãi khi mua đồ theo set nhé.', '2018-09-29 04:53:23', '2018-09-29 05:33:18'),
(2, 8, 'Gợi ý mix đồ công sở đẹp cho mùa thu', 'Thời tiết mùa thu có thể nói là những ngày dễ chịu nhất trong năm, cũng là gu dễ tính nhất đối với các bạn gái khi lựa đồ đi làm hay đi chơi. Những ngày thu mát dịu bạn gái có thể chọn mặc một chiếc sơ mi cộc tay hay dài tay đều phù hợp.', 'Thời tiết mùa thu có thể nói là những ngày dễ chịu nhất trong năm, cũng là gu dễ tính nhất đối với các bạn gái khi lựa đồ đi làm hay đi chơi. Những ngày thu mát dịu bạn gái có thể chọn mặc một chiếc sơ mi cộc tay hay dài tay đều phù hợp. Cô nàng cũng có thể mặc quần tây hoặc chân váy cũng đều cảm thấy thoải mái hay cả những chiếc quần jean ôm cũng không hề làm bạn cảm thấy khó chịu.\r\n\r\nĐặc biệt với thời trang thu bạn gái hoàn toàn có thể tận dụng nhiều set đồ từ hè sang được nhè, nếu bạn còn vẫn băn khoăn chưa biết mặc gì cho những ngày thu này thì để Tbiz gợi ý cho bạn nhé!\r\n* Chân váy + Sơ mi\r\nSet sơ mi và chân váy có lẽ khá quen thuộc với chị em công sở. Một chiếc sơ mi màu họa tiết có thể dễ dàng kết hợp với các mẫu chân váy đen đều nổi bật và ăn ý. Ngoài chân váy ôm thì cô nàng điệu hơn với một chiếc chân váy dáng xòe xếp ly này nhé, kiểu dáng trẻ trung cách tân được chọn kết hợp cùng sơ mi cổ tim cho set đồ mua thu chuẩn công sở nữ tính bạn gái mặc xinh cùng áo sơ mi dáng tay chuông, gam đen nổi bật cùng kiểu cổ đính hạt nổi. Và cô nàng quyến rũ hơn với set đồ cùng chân váy ôm xòe kết hợp sơ mi voan kiểu cách điệu.\r\nVẫn là bộ kết hợp quen thuộc nhưng với nhiều kiểu sơ mi khác nhau kết hợp cùng nhiều mẫu sơ mi khác nhau bạn vẫn trở nên nổi bật và ấn tượng nhé.\r\n* Sơ mi + Quần tây\r\n-Cá tính, trẻ trung từ kiểu dáng đến cách phối màu trong set đồ này cho bạn gái đến công sở. Một chiếc sơ mi đen tay lỡ kết hợp cùng quần ống lỡ họa tiết kẻ\r\n-Set đồ trẻ trung tươi trẻ cho những ngày thu với áo sơ mi họa tiết phồi màu nổi bật, mix quần tây dán baggy phong cách cực đẹp cho set đồ đến công sở\r\n-Áo sơ mi trắng bạn gái có thể dễ dàng kết hợp với nhiều màu quần để có set đồ nổi bật\r\n\r\nNhững set đồ đẹp, trẻ trung các cô nàng có thể dễ dàng chọn kết hợp mặc công sở hay tận dụng diện đi chơi đều hợp mốt, tại LOZA có đầy đủ toàn bộ các sản phẩm trên cho bạn gái lựa chọn nhé!', '2018-09-29 04:58:11', '2018-09-29 05:33:24'),
(3, 9, 'Cách diện quần tây đẹp mùa hè 2018', 'Thời trang hè của những cô nàng điệu vốn quen thuộc với những chiếc váy đầm điệu thoải mái cho ngày nắng. Nhưng đôi khi một set đồ đẹp thanh lịch cùng áo sơ mi thì lại không thể thiếu một chiếc chân váy hay quần tây.', 'Thời trang hè của những cô nàng điệu vốn quen thuộc với những chiếc váy đầm điệu thoải mái cho ngày nắng. Nhưng đôi khi một set đồ đẹp thanh lịch cùng áo sơ mi thì lại không thể thiếu một chiếc chân váy hay quần tây. Đừng để mình nhàm chán cả tuần với một style quen thuộc mà hãy luôn đổi mới để tạo cảm hứng làm việc cho cả tuần nhé.\r\n\r\nVới quần tây công sở bạn gái rất dễ để mix đồ cùng một chiếc sơ mi cổ đức thanh lịch hay một chiếc sơ mi cách điệu xinh xắn. Đặc biệt với thời trang công sở hiện đại thì những chiếc quần tây đã được cách tân khá nhiều cho các cô nàng tự tin diện đồ và kết hợp phong cách trẻ trung.\r\nQuần tây dáng ống lỡ với gam vàng bò sang chảnh rất thích hợp cho cô nàng diện các set đồ trẻ trung, hoặc cũng rất hợp phong cách nếu chọn mix cùng áo sơ mi cổ đức.\r\nQuần tây nữ công sở đẹp, trẻ trung với kiểu ống lửng. Cô nàng chọn set cùng sơ mi tay lỡ cân đối duyên dáng công sở.\r\nCô nàng cá tính khó mà mà làm ngơ trước một chiếc quần kẻ cực đẹp này nhé, kiểu dáng ống lỡ trẻ trung với gam màu ghi sang được chọn set ăn ý với áo sơ mi trắng cổ xếp lý. \r\nQuý cô sang chảnh với một chiếc quần tây dáng ống suông lỡ cho mùa hè, vẫn thoải mái như diện một chiếc váy voan mà lại thỏa sức mix đồ cá tính.\r\nQuần tây màu đen vốn chuẩn công sở nhưng được phá cách với kiểu dáng ống suông này đấy nhé. vẫn thanh lịch lại sang chảnh hơn nhiều.\r\nNhững chiếc quần âu màu đen trở nên trẻ trung hơn nhiều với các kiểu dáng cách tân nhé, mix đồ đẹp nổi bật với sơ mi họa tiết\r\nphong cách cổ điển với kiểu quần tây dáng ống đứng, chọn set cùng nhiều mẫu áo sơ mi họa tiết cô nàng vẫn tự tin mặc đẹp công sở với quần tây nhé!\r\n\r\nVới mỗi cô nàng công sở thì quần tây vẫn là món đồ cần phải có trong tủ đồ, nhưng mặc sao cho đẹp, cho trẻ trung thì đừng bỏ lỡ những gợi ý từ Tbiz nhé!', '2018-09-29 05:02:29', '2018-09-29 05:33:30'),
(4, 0, 'Những siêu phẩm áo sơ mi phong cách Hàn Quốc mê hoặc nàng công sở', 'Những cô nàng công sở khó mà bỏ qua một chiếc áo sơ mi đẹp cho các set đồ để đi làm, mặc dù trong tủ đồ của bạn đủ các mẫu váy đầm quyến rũ trẻ trung thì sơ mi đôi khi vẫn tiện dụng hơn nhiều với thời trang văn phòng.', 'Những cô nàng công sở khó mà bỏ qua một chiếc áo sơ mi đẹp cho các set đồ để đi làm, mặc dù trong tủ đồ của bạn đủ các mẫu váy đầm quyến rũ trẻ trung thì sơ mi đôi khi vẫn tiện dụng hơn nhiều với thời trang văn phòng.\r\n\r\nBỏ qua những mẫu đơn điệu mà cùng Tbiz ngắm nhìn BST áo sơ mi cách tân đẹp, mới lạ đậm phong cách thời trang Hàn Quốc đang làm mưa làm gió hè 2018 này nhé!\r\nMột chiếc sơ mi trắng chắc hẳn quá quen thuộc với dân công sở, nhưng hãy thử kiểu dáng này nhé, với thiết kế cổ đức thanh lịch nhưng dáng áo ngắn tạo độ eo gọn tiện dụng, mix chuẩn cùng quần tây nhưng trông cô nàng công sở chất hơn nhiều\r\nmùa hè này các cô nàng đừng bỏ lỡ một chiếc áo sơ mi họa tiết hoa đấy nhé, mix đẹp và năng động cùng quần jeans\r\nset đồ công sở đẹp mê hoặc với chân váy bút chì dáng dài được thiết kể xẻ trước đầy quyến rũ, kết hợp đẹp cùng áo sơ mi kiểu cổ lệch điệu.\r\nChuẩn phong cách công sở Hàn Quốc với chân váy bút chì ôm dáng dài mix đồ cực xinh cùng áo sơ mi họa tiết tay lỡ. Cả set đồ là gam màu nền pastel nhẹ nhàng rất thích hợp cho những ngày nắng nóng.\r\nCá tính cho nàng công sở với set đồ sơ mi kẻ cùng quần tây trắng dáng suông cực chất, một chiếc sơ mi kẻ sọc che khuyết điểm với dáng tay xòe điệu hơn.\r\nNhững chiếc sơ mi trắng thỏa sức mix đồ với những mẫu chân váy màu. Chọn kiểu dáng cách điệu để đổi mới style thời trang của mình nhé!\r\nThời trang hè năm nay có khá nhiều mẫu thiết kế mới, trẻ trung dành cho các cô nàng công sở, bỏ qua gu cổ điển mà hãy để mình trở nên năng động hơn, cá tính hơn bằng cách chọn các thiết kế độc đáo và mix đồ thật ấn tượng nhé!\r\nSơ mi mùa hè thiết kế rộng, thoáng tạo cảm giác thoải mái cho người mặc trong những ngày hè nóng.', '2018-09-29 05:05:19', '2018-09-29 05:05:19'),
(5, 9, '3 cách mix đồ đẹp đến công sở hè này với áo sơ mi', 'Những chiếc sơ mi công sở cực xinh cho mùa hè sẽ trở nên cuốn hút hơn nhiều nếu bạn biết khéo léo mix đồ, áo sơ mi cũng là món đồ dễ tính và đa dụng nhất khi bạn có thể dễ dàng kết hợp theo nhiều phong cách khác nhau.', 'Những chiếc sơ mi công sở cực xinh cho mùa hè sẽ trở nên cuốn hút hơn nhiều nếu bạn biết khéo léo mix đồ, áo sơ mi cũng là món đồ dễ tính và đa dụng nhất khi bạn có thể dễ dàng kết hợp theo nhiều phong cách khác nhau. Một chiếc sơ mi có thể phối cùng quần jean năng động hay chân váy ôm thanh lịch, trẻ trung hơn nếu phối với chân váy dáng xòe.\r\n\r\nPhong cách hè 2018 này nổi trội với nhiều mẫu áo sơ mi họa tiết xinh cho bạn gái lựa chọn. Cùng Tbiz gợi ý cho bạn 3 cách mix đồ cực đẹp với áo sơ mi cho những set đồ công sở mùa này nhé!\r\n\r\n  *  Áo sơ mi mix quần jean\r\n\r\nĐây là set đồ năng động nhất cho những chiếc áo sơ mi công sở, quần jean mặc hè có vẻ nóng bức nhưng lại rất tiện dụng.\r\nMột chiếc sơ mi họa tiết hoa xinh kiểu cổ nơ dễ mặc được cô nàng chọn kết hợp cùng quần jean ống ôm cực tôn dáng\r\ncô nàng sành điệu với áo sơ mi dáng xòe mix cùng quần jean trắng xinh.\r\nMột chiếc quần jean ống loe cá tính cho cô nàng mặc công sở, kết hợp đẹp với áo sơ mi trắng cổ trụ ngắn.\r\nMột chiếc sơ mio họa tiết kết hợp cùng quần jean không chỉ thích hợp đi làm mà cô nàng mặc đi chơi cũng rất hợp mốt trẻ trung đấy nhé!\r\n\r\n* Sơ mi mix cùng chân váy xòe\r\n\r\nStyle trẻ trung nhất của những chiếc sơ mi, đặc biệt với những chiếc sơ mi cách điệu sẽ rất hợp cho các set đồ cùng chân váy xòe\r\ncô nàng nổi bật dù xuống phố hay đến công sở với một chiếc áo sơ mi cách điệu màu đỏ rực được mix với chân váy xòe họa tiết\r\nsơ mi trắng thanh lịch luôn làm nền cho các mẫu chân váy xòe họa tiết.\r\n\r\n* Áo sơ mi mix chân váy ôm\r\n\r\nChân váy ôm kết hợp sơ mi vốn là set đồ thanh lịch quen thuộc của chị em công sở, nhưng với kiểu dáng và họa tiết được lựa chọn tinh tế thì cô nàng vẫn đủ nổi bật cuốn hút với các set đồ này.\r\nÁo sơ mi đỏ và chân váy hoa xinh nổi bật cho ngày hè.\r\nNhững mẫu áo sơ mi đẹp và các cách mix đồ khéo léo đáng để bạn diện cho mùa hè này rồi nhé.', '2018-09-29 05:07:59', '2018-09-29 05:33:38'),
(6, 9, '15 Mẫu đầm xòe cực xinh chào hè 2018', 'Đầm xòe hè 2018 có đủ kiểu dáng, đủ màu sắc, đủ họa tiết bắt mắt và tính ứng dụng cao khi các cô nàng có thể mặc đi làm, đi chơi, đi hẹn hò đều hợp mốt. Còn chần chừ gì nữa các nàng hãy nhanh tay chọn cho mình một chiếc đầm xòe cực xinh để diện hè này nhé!', 'Đầm xòe hè 2018 có đủ kiểu dáng, đủ màu sắc, đủ họa tiết bắt mắt và tính ứng dụng cao khi các cô nàng có thể mặc đi làm, đi chơi, đi hẹn hò đều hợp mốt. Còn chần chừ gì nữa các nàng hãy nhanh tay chọn cho mình một chiếc đầm xòe cực xinh để diện hè này nhé!\r\nCô nàng tiểu thư thích phong cách vintage có thể chọn cho mình chiếc đầm xòe dáng dài phối họa tiêt cực xinh này nhé, kiểu cổ V cuốn hút hơn.\r\nĐầm hoa nổi bật và không lo lỗi mốt nhé các bạn gái, chọn ngay cho mình chiếc đầm họa tiết hoa dáng xòe với kiểu thắt đai eo tôn dáng diện mùa hè này, chọn kiểu dáng này còn khá thích hợp cho những chuyến đi chơi biển nữa đấy nhé.\r\nPhong cách quý phái với chiếc đầm dáng xòe kiểu tay lỡ được tạo xếp vạt tạo cổ V, gam màu hồng tím đủ ổi bật mùa hè này.\r\nĐầm voan xòe điệu với dáng tay bèo, họa tiết hoa nhí bắt măt đủ xinh xuống phố hay đi chơi.\r\nMột chiếc váy hoa xòe dáng tay cộc là lựa chọn chuẩn cho những ngày hè, váy xòe dễ mặc lại khá che khuyết điểm.\r\nĐầm thời trang hè có nhiều kiểu dáng cho các bạn gái lựa chọn, một chiếc đầm xòe họa tiết hoa là lựa chọn tối ưu bởi sự trẻ trung, nổi bật rất hợp mốt.\r\nCô nàng màu nắng với chiếc váy xòe màu vàng chanh cực xinh cho các bạn gái diện đi phố\r\nHọa tiết chấm bi chưa bao giờ thôi hot cho các cô nàng công sở. Chiếc đầm xòe voan dáng xếp vạt đủ đẹp, đủ xinh cho bạn gái đi chơi\r\nnếu bạn luôn thích sự nổi bật thì đừng bảo giơ bỏ lỡ một chiếc đầm đỏ dáng xòe này nhé!\r\nThời trang hè thì lựa chọn đầm dáng xòe là lý tưởng cho các cô nàng, với cực nhiều mẫu đầm dáng xòe với thiết kế họa tiết hoặc những gam màu đơn nhưng đủ sự nổi bật cho bạn gái diện đi làm hoặc đi chơi.', '2018-09-29 05:10:23', '2018-09-29 05:33:50'),
(7, 0, 'Áo sơ mi cổ nơ chưa bao giờ lỗi mốt công sở', 'Những chiếc áo sơ mi cách điệu như dáng cổ nơ chưa bao giờ là nhàm, chưa bao giờ lỗi mốt với phái đẹp, đặc biệt với các bạn nữ công sở trẻ. Chọn một chiếc áo sơ mi cổ nơ duyên dáng được phối cùng chân váy hoặc quần tây là cô nàng đã có set đồ cực xinh cho thời trang công sở.', 'Những chiếc áo sơ mi cách điệu như dáng cổ nơ chưa bao giờ là nhàm, chưa bao giờ lỗi mốt với phái đẹp, đặc biệt với các bạn nữ công sở trẻ. Chọn một chiếc áo sơ mi cổ nơ duyên dáng được phối cùng chân váy hoặc quần tây là cô nàng đã có set đồ cực xinh cho thời trang công sở.\r\nÁo sơ mi cổ nơ cũng vẫn có khá nhiều dáng như kiểu nơ bản to hoặc dây nơ nhỏ duyên dáng cho các bạn gái lựa chọn để set đồ hợp mốt. Hè 2018 này áo sơ mi cổ nơ được phối đủ màu, đủ họa tiết thỏa sức các cô nàng lựa chọn. Cùng chiêm ngưỡng BST cực đẹp, những mẫu sơ mi cổ nơ hót nhất, thời thượng nhất thời trang nhé!\r\nÁo sơ mi họa tiết phối màu đỏ nổi bật với thiết kế cổ dây nơ cực điệu cho nàng. Dáng áo thụng kết hợp đẹp cùng các mẫu chân váy ôm.\r\nmột chiếc sơ mi cổ nơ mùa hè thích hợp với kiểu tay ngắn. Những mẫu họa tiết hoa nhí chưa bao giờ lỗi mốt và đặc biệt rất dễ mặc, dễ phối đồ cho các set đồ hè.\r\nmàu xanh tôn da và sang, với kiểu họa tiết hoa nhí xinh cho nàng mix đẹp với chân váy bút chì ôm.\r\nNhững chiếc sơ mi cổ nơ điệu, rất thích hợp với cô nàng vòng cổ thô mặc sẽ không bị lộ khuyết điểm, với những mẫu sơ mi màu đơn hay họa tiết đều khá hợp cho kiểu cổ nơ xinh.', '2018-09-29 05:12:31', '2018-09-29 05:12:31'),
(8, 9, 'Bí quyết để hết hè vẫn dùng được áo phông mỏng', 'Áo phông có thể là “đặc sản” của mùa hè nhưng không có nghĩa chỉ được diện vào mỗi mùa nóng', 'Áo phông vốn là “đặc sản” của mùa hè nhưng điều ấy không có nghĩa là bạn phải gói ghém chúng vào một góc nào đó trong tủ, bởi item này vẫn có tính ứng dụng không hề nhỏ trong suốt mùa thu đông sắp tới, chỉ cần bạn muốn!\r\n\r\nVới chất liệu mềm mại, kiểu dáng trẻ trung, áo phông rất hợp để mix cùng vô số trang phục khác để tạo nên những bộ cánh hứa hẹn không bao giờ lỗi mốt.\r\n\r\nDưới đây là một vài gợi ý tận dụng áo phông hè cho mùa lạnh đang ở ngay trước mắt:\r\n\r\n1. Mặc với váy áo hai dây để giữ ấm\r\n\r\n\r\n    Trang chủ\r\n\r\n    >> Thời trang \r\n\r\n     >> Xu hướng thời trang\r\n\r\n    Thời trang công sở\r\n    Bí quyết mặc đẹp\r\n    Xu hướng thời trang\r\n    Người mẫu nữ\r\n    Thời trang nam\r\n    Thời trang bốn mùa\r\n\r\nBí quyết để hết hè vẫn dùng được áo phông mỏng\r\nThứ Hai, ngày 17/09/2018 18:09 PM (GMT+7)\r\nSự kiện: Xu hướng thời trang	\r\nMách bạn một vài gợi ý tận dụng áo phông hè cho mùa lạnh đang ở ngay trước mắt.\r\nChia sẻ trên Fanpage Chia sẻ bài viết này trên trên Facebook Chia sẻ bài viết này trên trên G+\r\n\r\nBí quyết để hết hè vẫn dùng được áo phông mỏng - 1\r\n\r\nÁo phông có thể là “đặc sản” của mùa hè nhưng không có nghĩa chỉ được diện vào mỗi mùa nóng\r\n\r\nÁo phông vốn là “đặc sản” của mùa hè nhưng điều ấy không có nghĩa là bạn phải gói ghém chúng vào một góc nào đó trong tủ, bởi item này vẫn có tính ứng dụng không hề nhỏ trong suốt mùa thu đông sắp tới, chỉ cần bạn muốn!\r\n\r\nVới chất liệu mềm mại, kiểu dáng trẻ trung, áo phông rất hợp để mix cùng vô số trang phục khác để tạo nên những bộ cánh hứa hẹn không bao giờ lỗi mốt.\r\n\r\nDưới đây là một vài gợi ý tận dụng áo phông hè cho mùa lạnh đang ở ngay trước mắt:\r\n\r\n1. Mặc với váy áo hai dây để giữ ấm\r\n\r\nBí quyết để hết hè vẫn dùng được áo phông mỏng\r\n\r\nNhững chiếc váy và áo hai dây thực sự rất đẹp, nhưng trong một năm, bạn có khi chỉ có chừng một vài tháng để mặc chúng một mình. Một khi mùa lạnh tới, bạn buộc phải cất chúng đi, đợi tới mùa hè năm sau, mà cũng chưa chắc đã có cơ hội mặc lại vì quần áo giờ đây rất nhanh hết mốt. Song nếu có thêm một chiếc áo phông lại khác! Bạn có thể kết hợp váy áo hai dây đặc trưng của mùa hè với áo thun để tạo thành set đồ mùa thu, đủ điệu đà, đủ nữ tính và quan trọng là vẫn đủ ấm.\r\n\r\n2. Mix với chân váy dài, dày dặn\r\nAi đó thích mặc kiểu “trên đông dưới hè” thì mặc họ, còn bạn cứ theo công thức “trên hè dưới đông”, đảm bảo hợp thời trang lại hợp cả thời tiết. Hãy mix những chiếc áo phông mát rười rượi của mùa hè với chân váy mùa đông, loại vừa dài, lại vừa dày dặn. Bạn chẳng những có thể giữ ấm cho toàn bộ phần thân dưới mà còn vô cùng linh động khi chỉ cần thêm một chiếc áo khoác bất kỳ là có ngay set đồ chống chọi với mùa lạnh một cách công hiệu.\r\n\r\n3. Mặc với suit thì quá hoàn hảo\r\nTrong lúc bạn còn mải nghĩ xem nên mặc gì với bộ suit để trông bớt đi vẻ cứng nhắc, già nua thì có rất nhiều quý cô nhanh chân đã tận dụng ngay chiếc áo phông quen thuộc để phối cùng những bộ suit cổ điển. Mà bạn biết đấy, mùa thu đông chính là thời điểm thích hợp nhất để diện suit chứ còn gì nữa!\r\n\r\n4. Công thức áo phông và sơ mi kẻ chưa bao giờ cũ\r\nTừ chối một công thức kinh điển như áo phông và sơ mi kẻ khoác ngoài đồng nghĩa với việc tước đi của bạn nhiều phần trăm cơ hội mặc đẹp! Công thức này tuy không mới, song chưa bao giờ cũ kỹ theo cả nghĩa đen lẫn nghĩa bóng. Nó giúp bạn trông năng động hơn, ứng phó hiệu quả hơn với những cơn gió ngày giao mùa. Để set đồ dễ dàng phát huy tính thời trang, bạn có thể chọn kiểu áo phông dáng hơi ôm rồi mix cùng sơ mi kẻ dáng oversize.', '2018-09-29 05:13:51', '2018-09-29 05:34:00'),
(9, 8, 'Đâu là xu hướng sang chảnh nhất của Thu Đông năm nay?', 'Hãy thử đoán xem xu hướng nào sẽ thống lĩnh mùa Thu Đông 2018?', 'Mùa mới, là dịp để chọn những xu hướng thanh lịch nhất cho tủ đồ. \r\nMùa hè năm nay tràn ngập những xu hướng trẻ trung và không thực sự cao cấp như áo/mũ nhựa trong suốt, trang sức vỏ sò, túi đính hạt, màu neon, tóc nhuộm khói... Thế nên, mùa mới là dịp để các cô nàng \"tút\" lại tủ đồ, để cập nhật những xu hướng thanh lịch và sang chảnh cho mùa mới.\r\n\r\nTừ sàn diễn thời trang Thu Đông 2018, có thể thấy các nhà thiết kế đang dành hết sự chú ý cho những xu hướng cao cấp, và có một xu hướng được đảm bảo sẽ khiến mọi món đồ trở nên sang trọng hơn: phủ da cá sấu. Da cá sấu vốn chỉ xuất hiện ở phụ kiện, năm nay thống lĩnh các sàn diễn từ Louis Vuitton, Marni, Tory Burch, Versace... Không nằm ngoài xu hướng, các hãng thời trang bán lẻ bình dân cũng đã biến những món đồ da cá sấu thành chủ đạo trong bộ sưu tập Thu Đông của mình. \r\n\r\n* Từ sàn diễn thời trang\r\nRejiya Pyo F/W 2018\r\nTory Burch F/W 2018\r\nMarni F/W 2018\r\nLouis Vuitton F/W 2018\r\n* Tới các bộ sưu tập ứng dụng\r\nThe Kooples $545\r\nPhillip Lim $495\r\nAnderson $155\r\nTúi Pop & Suki $217\r\nMiista $290', '2018-09-29 05:16:30', '2018-09-29 05:34:17'),
(10, 0, 'Có một công thức mặc đẹp \"nghìn năm\" vẫn chưa lỗi mốt!', 'Bạn có phải là một fan mê mẩn công thức \"white on white\" - cả cây trắng?', 'Có một công thức mặc đẹp vẫn luôn trụ vững trong lòng các tín đồ thời trang dù bao mùa đi qua, bao mốt mới lên rồi lại xuống - đó là \"white on white\" (cả cây trắng). Chẳng phải vô lý mà công thức mix đồ này dù mùa Tuần lễ thời trang nào cũng được các cô nàng sành điệu khắp nơi trên thế giới diện đi diện lại. Và với bản lĩnh của những cô nàng đi đầu xu hướng, dường như chẳng bao giờ chúng bị nhàm chán cả. \r\n\r\nTừ những set đồ đậm chất street style\r\n\r\nĐó là khi các tín đồ chỉ mix áo phông và quần jeans trắng, thêm kính râm và túi xách cùng vài phụ kiện là đã có một set đồ khí chất ngời ngời. \r\nTới những set đồ bánh bèo kiêu kì như công chúa\r\n\r\nNếu diện cả cây trắng mà thiếu những chiếc váy bánh bèo kiêu kì thì thật là phí phạm. Và tất nhiên, các cô nàng tín đồ thời trang đình đám sẽ không bỏ qua công thức tuyệt đẹp này. \r\nHay phong cách tối giản mà trang nhã\r\n\r\nĐây chính là những set đồ bạn có thể áp dụng tới công sở. Dù là blazer, sơ mi oversize hay áo khoác jeans, các tín đồ thời trang đều \"cân\" được hết.', '2018-09-29 05:17:57', '2018-09-29 05:17:57'),
(11, 8, 'Có một gam màu sinh ra dành cho mùa thu!', 'Vừa ngọt ngào nữ tính lại theo chân bạn được đến bất cứ nơi đâu!', 'Những khối màu rực rỡ - color block có lẽ sẽ tới lúc làm bạn thấy mệt mỏi và e ngại vì sự chói lóa, những gam màu kẹo ngọt như hồng có thể khiến bạn thấy e dè vì quá bánh bèo, tương tự như vàng mù tạt có thể rất bắt mắt nhưng không phải cô nàng nào cũng muốn mình thành tâm điểm sự chú ý... Nếu bạn rơi vào những trường hợp như vậy, hãy thử để ý tới màu xanh pastel xem sao?\r\n\r\nNói là xanh pastel nhưng có rất nhiều tông màu, từ xanh baby blue ngọt ngào thơ mộng, xanh cổ vịt thâm trầm hay xanh xám làm dịu những ngày nắng gắt... Chẳng phải đây là gam màu rất thích hợp cho những ngày mùa thu hay sao?\r\n\r\nVới các cô nàng công sở luôn tìm kiếm sự lịch thiệp và trang nhã, những chiếc sơ mi xanh xám hay váy xanh ngọc mix cùng đồ trắng sẽ là công thức không bao giờ làm bạn thất vọng. \r\nVới những cô nàng tìm kiếm một món đồ giúp làm mới tủ đồ đã chật cứng với váy vàng mù tạt, áo đỏ, váy hồng... thì xanh pastel lại đem đến một không khí tươi mát, như một luồng gió mới cho phong cách Thu. Chẳng cần mua sắm quá nhiều mà trông bạn như vừa trải qua cuộc cách tân về phong cách vậy.', '2018-09-29 05:18:52', '2018-09-29 05:34:29');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sale` int(11) NOT NULL DEFAULT '0',
  `sex` tinyint(1) NOT NULL DEFAULT '0',
  `view` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `category_id`, `image`, `price`, `quantity`, `description`, `short_description`, `sale`, `sex`, `view`, `created_at`, `updated_at`) VALUES
(1, 'Áo Khoác Bomber Kaki Màu Bò', 3, 'storage/products/ao-khoac-bomber-kaki-mau-bo-ak193-4918-p.jpg', 390000, 30, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 30, 1, 1, '2018-09-29 04:13:38', '2018-09-29 15:25:41'),
(2, 'Áo Khoác Nỉ Xám Chuột', 3, 'storage/products/ao-khoac-ni-xam-chuot-ak174-2176-p.jpg', 345000, 91, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 'Lorem Ipsum chỉ đơn giản là một đoạn văn bản giả, được dùng vào việc trình bày và dàn trang phục vụ cho in ấn. Lorem Ipsum đã được sử dụng như một văn bản chuẩn cho ngành công nghiệp in ấn từ những năm 1500, khi một họa sĩ vô danh ghép nhiều đoạn văn bản với nhau để tạo thành một bản mẫu văn bản. Đoạn văn bản này không những', 50, 1, 0, '2018-09-29 04:19:06', '2018-09-29 15:25:53'),
(3, 'Quần Jean Đen', 3, 'storage/products/quan-jean-den-qj1435-6402-p.jpg', 400000, 99, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 'Lorem Ipsum chỉ đơn giản là một đoạn văn bản giả, được dùng vào việc trình bày và dàn trang phục vụ cho in ấn. Lorem Ipsum đã được sử dụng như một văn bản chuẩn cho ngành công nghiệp in ấn từ những năm 1500, khi một họa sĩ vô danh ghép nhiều đoạn văn bản với nhau để tạo thành một bản mẫu văn bản. Đoạn văn bản này không những', 20, 1, 2, '2018-09-29 04:24:43', '2018-09-29 15:26:02'),
(4, 'Quần Jean Xanh', 3, 'storage/products/quan-jean-xanh-qj1450-6430-p.jpg', 400000, 31, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 10, 1, 0, '2018-09-29 04:31:16', '2018-09-29 15:26:08'),
(5, 'Áo Thun TAY Dài Đỏ Đô', 3, 'storage/products/ao-thun-tay-dai-do-do-at544-984-p.jpg', 185000, 91, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 25, 1, 0, '2018-09-29 04:33:14', '2018-09-29 15:26:16'),
(6, 'Áo Sơ Mi Xanh Ngọc Đậm', 3, 'storage/products/ao-so-mi-xanh-ngoc-dam-phoi-co-asm647-1004-p.jpg', 225000, 95, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 50, 1, 0, '2018-09-29 04:35:42', '2018-09-29 15:26:23'),
(7, 'Áo Thun Cổ Tròn Đỏ Đô', 3, 'storage/products/ao-thun-co-tron-do-do-at649-5996-p.jpg', 145000, 43, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 0, 1, 0, '2018-09-29 04:41:15', '2018-09-29 15:26:33'),
(8, 'Quần Short Jean Xanh Dương', 3, 'storage/products/quan-short-jean-xanh-duong-qs57-1877-p.jpg', 265000, 82, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 30, 1, 0, '2018-09-29 04:44:57', '2018-09-29 15:26:40'),
(9, 'Quần Tây Đỏ Cam', 3, 'storage/products/quan-tay-do-cam-qt55-4904-p.jpg', 295000, 87, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 0, 1, 0, '2018-09-29 04:47:19', '2018-09-29 15:26:48'),
(10, 'Áo Thun Cổ Tròn Cam', 3, 'storage/products/ao-thun-co-tron-cam-at596-3379-p.jpg', 175000, 58, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 0, 1, 0, '2018-09-29 04:49:03', '2018-09-29 15:26:56'),
(11, 'Nón Màu Cafe', 4, 'storage/products/non-mau-cafe-n231-2862-p.jpg', 95000, 23, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 5, 1, 1, '2018-09-29 05:00:52', '2018-09-29 15:27:10'),
(12, 'Nơ Đeo Cổ Trắng', 4, 'storage/products/no-deo-co-trang-no74-4520-p.jpg', 65000, 32, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 0, 1, 0, '2018-09-29 05:03:17', '2018-09-29 05:03:17'),
(13, 'Mắt Kính Xanh Nam', 4, 'storage/products/mat-kinh-xanh-nam-mk109-881-p.jpg', 175000, 31, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 0, 1, 0, '2018-09-29 05:04:06', '2018-09-29 05:04:06'),
(14, 'Nón Snapback Trắng', 4, 'storage/products/non-snapback-trang-n290-5555-p.JPG', 115000, 61, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 20, 1, 0, '2018-09-29 05:06:38', '2018-09-29 05:07:37'),
(15, 'Túi Xách Nâu', 4, 'storage/products/tui-xach-nau-tx87-6323-p.jpg', 425000, 25, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 0, 1, 0, '2018-09-29 05:08:51', '2018-09-29 05:08:51'),
(16, 'Cà Vạt Hàn Quốc Đen', 4, 'storage/products/ca-vat-han-quoc-den-cv47-1288-p.jpg', 145000, 72, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 0, 1, 0, '2018-09-29 05:11:05', '2018-09-29 05:11:05'),
(17, 'Nón Đen N274', 4, 'storage/products/non-den-n274-5360-p.jpg', 115000, 29, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 0, 1, 1, '2018-09-29 05:12:11', '2018-09-29 15:18:12'),
(18, 'Cà Vạt Hàn Quốc Xanh Đen', 4, 'storage/products/ca-vat-han-quoc-xanh-den-cv32-3552-p.jpg', 145000, 58, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 30, 1, 0, '2018-09-29 05:13:22', '2018-09-29 05:13:22'),
(19, 'Nón Snapback Đen', 4, 'storage/products/non-snapback-den-n289-5549-p.jpg', 95000, 69, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 0, 1, 0, '2018-09-29 05:14:19', '2018-09-29 05:14:19'),
(20, 'Túi Xách Đen', 4, 'storage/products/tui-xach-den-tx74-3815-p.jpg', 425000, 88, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 10, 1, 0, '2018-09-29 05:16:04', '2018-09-29 05:16:04'),
(21, 'Giày Tây Đen', 5, 'storage/products/giay-tay-den-g125-6313-p-s-t.jpg', 645000, 88, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 0, 1, 0, '2018-09-29 05:17:11', '2018-09-29 05:17:11'),
(22, 'Giày Tây Đen', 5, 'storage/products/giay-tay-den-g116-5936-p.jpg', 595000, 80, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 0, 1, 0, '2018-09-29 05:18:23', '2018-09-29 05:18:23'),
(23, 'Giày Tây Đen', 5, 'storage/products/giay-tay-den-g129-6480-p.jpg', 575000, 99, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 0, 1, 0, '2018-09-29 05:19:32', '2018-09-29 05:19:32'),
(24, 'Giày Tây Đỏ', 5, 'storage/products/giay-tay-do-g65-3305-p.jpg', 575000, 78, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 0, 1, 0, '2018-09-29 05:20:21', '2018-09-29 05:20:21'),
(25, 'Giày Thể Thao Màu Đen', 5, 'storage/products/giay-the-thao-mau-den-g136-4285-p.jpg', 595000, 25, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 0, 1, 0, '2018-09-29 05:21:27', '2018-09-29 05:21:27'),
(26, 'Giày Tây Da Đen G93', 5, 'storage/products/giay-tay-da-den-g93-4286-p.jpg', 685000, 76, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 50, 1, 0, '2018-09-29 05:25:21', '2018-09-29 05:25:21'),
(27, 'Giày Tây Nâu', 5, 'storage/products/giay-tay-den-g129-6480-p.jpg', 575000, 45, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 0, 1, 0, '2018-09-29 05:26:48', '2018-09-29 05:26:48'),
(28, 'Giày Tây Đen', 5, 'storage/products/giay-tay-den-g132-6760-p.jpg', 675000, 53, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 10, 1, 0, '2018-09-29 05:27:37', '2018-09-29 05:27:37'),
(29, 'Giày Tây Đen', 5, 'storage/products/giay-tay-den-g67-3314-p.jpg', 545000, 36, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 30, 1, 0, '2018-09-29 05:28:36', '2018-09-29 05:28:36'),
(30, 'Giày Mọi Da Lộn Đen', 5, 'storage/products/giay-moi-da-lon-den-g95-4291-p.jpg', 545000, 28, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 0, 1, 0, '2018-09-29 05:29:30', '2018-09-29 05:29:30');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permission` int(11) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `permission`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', '$2y$10$DH5o.dvDWRaFLQqXfMDIlemRuqIL68CCaPRkJBcfkDrJHgSxwRzmi', 0, 'mdlvUJvHVlPXYUF6rhMmmMQtIL9Ql16G0YqenSQb1SmOUPnS8Jcugb3xg6Ya', '2018-09-29 04:04:48', NULL),
(2, 'Nam', 'nam@gmail.com', '$2y$10$ttzVK6KoF9lwVB0hCFoBaewoKYQipTib03cz2gBA5dDbuUsQ27NAC', 1, 'z5BKjc4yXNaLJe6uJpaKhrMHlJ2DFDZXuAnEFMSxPahYcTTaR8KDsYBSwQTW', '2018-09-29 05:49:17', '2018-09-29 05:49:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
