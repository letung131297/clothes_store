<!DOCTYPE html>
<html>
<head>
	<title>Hóa đơn</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
</head>
<body>
	<div class="container content-print-order" style="margin-top: 30px;">
		<div class="row">
			<div class="col-md-6">
				<h3>Tin Store</h3>
				<p>Địa chỉ: Tầng 18 tòa nhà ABC An Hòa Hà Đông, Hà Nội</p>
				<p>Hotline: 0123456789</p>
				<p>Email: TinStore&#64;gmail.com</p>
				<p>Website: tinstore.com</p>
			</div>
			<div class="col-md-6">
				<h3>HÓA ĐƠN BÁN HÀNG</h3>
				@if(!empty($order))
				<p>Mã đơn hàng: MĐH{{ $order->id }}</p>
				<p>Khách hàng: {{ $order->name }}</p>
				<p>Số điện thoại: {{ $order->phone }}</p>
				<p>Địa chỉ: {{ $order->address }}</p>
				<p>Ghi chú: {{ $order->description }}</p>
				@endif
			</div>
		</div>

		<div class="row">
			<table class="table table-inverse table-bordered text-center">
				<thead>
					<tr>
						<th>STT</th>
						<th>Mặt hàng</th>
						<th>Số lượng</th>
						<th>Đơn Giá</th>
						<th>Thành tiền</th>
					</tr>
				</thead>
				<tbody>
					<?php $stt = 1; ?> 
					@if(!empty($orderItems))
					@foreach($orderItems as $key => $item)
					<tr>
						<td>{{ $stt++ }}</td>
						<td>{{ $item->name }}</td>
						<td>{{ $item->quantity }}</td>
						<td>{{ number_format( $item->price, 0, ',', '.' ) }}đ</td>
						<td>{{ number_format( $item->price * $item->quantity, 0, ',', '.' ) }}đ</td>
					</tr>
					@endforeach
					@endif
				</tbody>
			</table>

		</div>

		<div class="row" style="margin-top: 30px;">
			<div class="col-md-4">
				<p class="text-center">Người lập hóa đơn</p>
			</div>
			<div class="col-md-8 text-center">
				<p class="">Tổng cộng: {{ number_format( $order->total, 0, ',', '.' ) }}đ</p>
				<p class="">Ngày ... Tháng ... Năm ...</p>
			</div>
		</div>
	</div>
</body>
</html>