<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href=" {{ route('adminDashboard') }} ">Clothes Store</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
  <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href=" {{ route('categories.index') }} ">Danh mục</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href=" {{ route('products.index') }} ">Sản phẩm</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href=" {{ route('orders.index') }} ">Đơn hàng</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href=" {{ route('posts.index') }} ">Tin tức</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href=" {{ route('indexStatistics') }} ">Thống kê</a>
      </li>
      <li class="nav-item dropdown">
            @auth
              <a class="nav-link dropdown-toggle username" href="#" id="logged" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Xin chào {{ \Auth::user()->name }}
              </a>
              <div class="dropdown-menu" aria-labelledby="logged">
                <a class="dropdown-item" href="{{ route('logout') }}"
                                 onclick="event.preventDefault();
                                               document.getElementById('logout-form').submit();">
                                  {{ __('Đăng xuất') }}
                              </a>

                              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                  @csrf
                              </form>
              </div>
              
                    @else
            <a class="nav-link dropdown-toggle" href="#" id="login-register" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Đăng nhập/Đăng ký
            </a>
            <div class="dropdown-menu" aria-labelledby="login-register">
              <a class="dropdown-item" href="{{ route('login') }}">Đăng nhập</a>
              <a class="dropdown-item" href="{{ route('register') }}">Đăng ký</a>
            </div>
                    @endauth
          </li>
    </ul>
  </div>
</nav>