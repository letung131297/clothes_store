<hr>
<div class="container footer">
	<div class="row">
		<div class="col-md-6">
			<ul class="list-info">
				<li>
					<i class="fa fa-home" aria-hidden="true" style="font-size: 18px;"></i> | 
					Tầng 18 tòa nhà ABC An Hòa Hà Đông
				</li>
				<li>
					<i class="fa fa-envelope" aria-hidden="true" style="font-size: 17px;"></i> | 
					TinStore@gmail.com
				</li>
				<li>
					<i class="fa fa-phone" aria-hidden="true" style="font-size: 20px;"></i> | 
					0123456789
				</li>
			</ul>
		</div>
		<div class="col-md-6">
			<ul class="list-social">
				<li><i class="fa fa-facebook"></i></li>
				<li><i class="fa fa-instagram"></i></li>
				<li><i class="fa fa-twitter"></i></li>
			</ul>
			<p class="copy-right"><i>Copyright by DevTeam 2018</i></p>
		</div>
	</div>
</div>
</body>
</html>