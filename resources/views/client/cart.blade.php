@extends('client.master')
@section('title')
Giỏ hàng
@endsection
@section('content')
<div class="container-fluid page-title">
	<div class="container">
		<p class="title">Giỏ hàng</p>
	</div>
</div>
<div class="container cart">
	@if(!empty($products))
	<form method="post" action="{{ route('updateCart') }}">
		@csrf
	<div class="row">
		<table class="table table-inverse col-md-12 list-product">
			<thead>
				<tr>
					<th>MÃ SẢN PHẨM</th>
					<th>ẢNH SẢN PHẨM</th>
					<th>TÊN SẢN PHẨM</th>
					<th>GIÁ SẢN PHẨM</th>
					<th>SỐ LƯỢNG</th>
					<th>THÀNH TIỀN</th>
				</tr>
			</thead>
			<tbody>
				
				@foreach($products as $product)
				<tr>
					<td>TS{{ $product['id'] }}</td>
					<td>
						<a href="{{ route('showProduct', ['id' => $product['id']]) }}">
							<img src="{{ $product['image'] }}" style="max-width: 100px;">
						</a>
					</td>
					<td>
						<a href="{{ route('showProduct', ['id' => $product['id']]) }}">
							{{ $product['name'] }}
						</a>
					</td>
					<td>{{ number_format($product['price'], 0, ',', '.') }}đ</td>
					<td><input type="number" name="cart[{{ $product['id'] }}]" value="{{ $product['cart_quantity'] }}"></td>
					<td>{{ number_format($product['price'] * (1- $product['sale']/100) * $product['cart_quantity'], 0, ',', '.') }}đ</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="float-left">
				<button type="submit" class="btn btn-success">Cập nhật giỏ hàng</button>
				<a href="{{ route('emptyCart') }}" class="btn btn-danger">Xóa giỏ hàng</a>
			</div>
			<div class="float-right text-right">
				<span>Tổng giá: </span><span class="total">{{ number_format($totalMoney, 0, ',', '.') }}đ</span>
				<div class=""><a href="{{ route('showCheckout') }}" class="btn btn-primary">Thanh toán</a></div>
			</div>
		</div>
	</div>
	</form>
	@endif
	@if(empty($products)) <center><p>Giỏ hàng chưa có gì.</p></center> @endif

</div>
@endsection