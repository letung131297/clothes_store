@extends('client.master')
@section('content')
<div id="sliderHome" class="carousel slide" data-ride="carousel">
	<div class="carousel-inner">
		<div class="carousel-item active">
			<img class="d-block w-100" src="{{ asset('uploads/sliders/slider-04.jpg') }}" alt="First slide">
		</div>
		<div class="carousel-item">
			<img class="d-block w-100" src="{{ asset('uploads/sliders/slider-06.jpg') }}" alt="Second slide">
		</div>
		<div class="carousel-item">
			<img class="d-block w-100" src="{{ asset('uploads/sliders/slider-02.jpg') }}" alt="Third slide">
		</div>
	</div>
	<a class="carousel-control-prev" href="#sliderHome" role="button" data-slide="prev">
		<span class="carousel-control-prev-icon" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>
	<a class="carousel-control-next" href="#sliderHome" role="button" data-slide="next">
		<span class="carousel-control-next-icon" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>
</div>
<style type="text/css">
	.carousel-item img {max-height: 550px;}
</style>
<div class="container">
	<div class="row favour">
		<div class="col-md-6 item-favour free-ship">
			<div class="wrapper">
				<span class="text">Free Ship Nội thành</span>
				<span class="icon-free-ship"><i class="fa fa-truck"></i></span>
			</div>
		</div>
		<div class="col-md-6 item-favour gift">
			<div class="wrapper">
				<span class="text">Quà tặng cuối tuần</span>
				<span class="icon-gift"><i class="fa fa-gift"></i></span>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
			@if(isset($total_search))
			<h3>Tìm thấy: {{ $total_search }} kết quả</h3>
			@endif
			@if(isset($title_category))
			<h3>{{ $title_category }}</h3>
			@endif
		</div>
		<div class="col-md-6">
			<div class="dropdown float-right">
			  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
			    Lọc sản phẩm
			  </button>
			  <div class="dropdown-menu">
			    <a class="dropdown-item" href="{{ route('productOrder', ['orderBy' => 'price', 'order' => 'asc']) }}">Theo giá từ thấp đến cao</a>
			    <a class="dropdown-item" href="{{ route('productOrder', ['orderBy' => 'price', 'order' => 'desc']) }}">Theo giá từ cao xuống thấp</a>
			    <a class="dropdown-item" href="{{ route('productOrder', ['orderBy' => 'sex', 'order' => '1']) }}">Theo giới tính: Nam</a>
			    <a class="dropdown-item" href="{{ route('productOrder', ['orderBy' => 'sex', 'order' => '0']) }}">Theo giới tính: Nữ</a>
			  </div>
			</div>
		</div>
	</div>
	<div class="row list-product">
		
		@foreach($products as $product)
		<div class="col-md-3 product">
			<div class="card">
				<img class="card-img-top" src="{{ asset($product->image) }}" alt="Card image cap">
				<div class="card-body">
					<h5 class="card-title">{{ $product->name }}</h5>
					<p class="card-text">
						@if($product->sale != 0)
						<span class="price-sale">
							{{ number_format(($product->price * $product->sale) / 100, 0, ',', '.') }}đ
						</span>
						<span class="price-primary">
							<del>{{ number_format($product->price, 0, ',', '.') }}đ</del>
						</span>
						@endif
						@if($product->sale == 0) 
							<span class="price-sale">
								{{ number_format($product->price, 0, ',', '.') }}đ
							</span>
						@endif
					</p>
					<a href="{{ route('showProduct', ['id' => $product->id]) }}" class="btn btn-success">Chi tiết</a>
				</div>
			</div>
		</div>
		@endforeach
		
	</div>
	
</div>
@endsection