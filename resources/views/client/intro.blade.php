@extends('client.master')
@section('title')
Giới thiệu
@endsection
@section('content')
<div class="container-fluid page-title">
	<div class="container">
		<p class="title"><strong>GIỚI THIỆU VỀ TIN STORE</strong></p>
	</div>
</div>
<div class="container introduce">

	<div class="row">
		<div class="col-md-12">
			<div class="content">
<p>Công ty TNHH Hoàng Long thành lập năm 2015 hoạt động trong lĩnh vực sản xuất hàng thời trang xuất khẩu với các sản phẩm chủ yếu làm từ len, sợi. Năm 2017, thương hiệu thời trang Tin Store được ra đời nhằm khai thác nhu cầu đang tăng lên của thị trường nội địa bằng các dòng sản phẩm len, sợi với mẫu mã phong phú và chất lượng cao. Với nỗ lực không ngừng trong việc đa dạng hoá các dòng sản phẩm để phục vụ nhu cầu ngày càng cao của Quý khách hàng, đến nay Tin Store đã có các dòng sản phẩm:</p>
	<ul>
		<li>Mùa đông với dòng sản phẩm thế mạnh len, sợi truyền thống và áo khoác vải ấm áp.</li>
		<li>Mùa hè với dòng sản phẩm thun, kaki thoáng mát.</li>
		<li>Các sản phẩm cho trẻ em gồm cả hàng len, sợi, cotton cho mùa đông và mùa hè.</li>
	</ul>
<p>Nắm bắt được nhu cầu sử dụng sản phẩm len, sợi trên thế giới ngày một lớn, trong khi đó các làng nghề truyền thống hoạt động trong lĩnh vực này qui mô sản xuất nhỏ lẻ, kỹ thuật dựa vào kinh nghiệm, quản lý không mang tính tập trung nên không thể đáp ứng những đơn đặt hàng lớn với yêu cầu kỹ thuật, hoàn tất chất lượng cao. Quyết tâm xây dựng 1 nhà máy sản xuất với đội ngũ công nhân công nghiệp, xây dựng hệ thống quản lý chất lượng theo phương pháp chủ động, công ty đã mạnh dạn đầu tư dài hạn về máy móc, công nghệ và đặc biệt là cử cán bộ đi học tập nước ngoài để nâng cao trình độ kỹ thuật và kinh nghiệm quản lý.</p>
<p>Sau những nỗ lực không ngừng của đội ngũ lãnh đạo và toàn thể cán bộ công nhân viên, năm 2018 công ty đã khánh thành nhà máy sản xuất mới tại khu CN Phố nối A với tổng diện tích 15.000m2, đưa vào sử dụng hơn 1200 máy chuyên dụng các loại với số lượng công nhân trực tiếp và vệ tinh lên tới con số 1000 người. Với năng lực sản xuất như hiện nay Công ty Cổ phần Thương mại và Dịch vụ Hoàng Dương hoàn toàn có thể đáp ứng được những đơn đặt hàng lớn với chất lượng cao, đồng đều và cam kết thời gian giao hàng đúng hạn. Bằng chất lượng và mẫu mã sản phẩm hàng len, sợi của công ty đã có mặt và chinh phục được khách hàng ở nhiều thị trường khó tính như: Đức, Mỹ, Đài Loan, CH Séc, Ba Lan, Anh,…</p>
			</div>
		</div>
	</div>
</div>
@endsection