@extends('client.master')
@section('title')
Thanh toán
@endsection
@section('content')
<div class="container-fluid page-title">
	<div class="container">
		<p class="title">Thanh toán</p>
	</div>
</div>
<div class="container checkout">
	<div class="row">
		<form action="{{ route('insertOrder') }}" method="POST" class="col-md-12">
			<div class="flex-1 info-customer">
				<h3 class="title-checkout">Thông tin khách hàng</h3>
				<div class="form-group">
					<label>Họ tên người nhận</label>
					<input type="text" name="name" class="form-control" value="{{ \Auth::user()->name }}">
				</div>
				<div class="form-group">
					<label>Số điện thoại</label>
					<input type="text" name="phone" class="form-control">
				</div>
				<div class="form-group">
					<label>Địa chỉ</label>
					<input type="text" name="address" class="form-control">
				</div>
				<div class="form-group">
					<label>Ghi chú</label>
					<textarea name="description" class="form-control"></textarea>
				</div>
			</div>
			<div class="flex-1 info-cart">
				<h3 class="title-checkout">Thông tin đơn hàng</h3>
				<table class="table table-inverse text-center">
					<thead>
						<tr>
							<th>Sản phẩm</th>
							<th>Số lượng</th>
							<th>Thành tiền</th>
						</tr>
					</thead>
					<tbody>
						@foreach($products as $product)
						<tr>
							<td>{{ $product['name'] }}</td>
							<td>x{{ $product['cart_quantity'] }}</td>
							<td>
								{{ 
									number_format($product['price'] * ( 1- $product['sale']/100) * $product['cart_quantity'], 0, ',', '.') 
								}}đ
							</td>
						</tr>
						@endforeach
						<tr class="total">
							<td></td>
							<td class="text">Tổng số tiền</td>
							<td class="price">{{ number_format($totalMoney, 0, ',', '.') }}đ</td>
							<input type="hidden" name="total" value="{{ $totalMoney }}">
						</tr>
						<tr>
							<td></td>
							<td>@csrf</td>
							<td><button type="submit" class="btn btn-success">GỬI ĐƠN HÀNG</button></td>
						</tr>
					</tbody>
				</table>
			</div>
		</form>
	</div>
</div>
@endsection