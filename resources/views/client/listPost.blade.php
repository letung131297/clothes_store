@extends('client.master')
@section('title')
Sản phẩm
@endsection
@section('content')
<div class="container-fluid page-title">
    <div class="container">
        <p class="title">{{ $title_category }}</p>
    </div>
</div>
<div class="container list-post">
    <div class="row">
        <div class="col-md-12">
            @foreach($posts as $post)
            <article>
                <h3>{{ $post->title }}</h3>
                <p class="short-description">
                    {{ $post->short_content }}
                </p>
                <a href="{{ route('showSinglePost', ['id' => $post->id]) }}">Xem tiếp</a>
            </article>
            @endforeach
        </div>
    </div>
    
</div>
@endsection