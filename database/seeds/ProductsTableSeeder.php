<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        DB::table('categories')->insert([
            'name' => 'Sản phẩm',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('categories')->insert([
            'name' => 'Tin tức',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        for ($i=0; $i < 10; $i++) { 
            DB::table('categories')->insert([
                'name' => str_random(10),
                'parent_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
        
        for ($i=0; $i < 20; $i++) { 
            DB::table('products')->insert([
                'name' => str_random(10),
                'category_id' => mt_rand(1, 10),
                'image' => "/uploads/product_images/600x600.png",
                'price' => mt_rand(100000, 900000),
                'quantity' => mt_rand(1, 10),
                'sale' => 0,
                'sex' => mt_rand(0, 1),
                'view' => mt_rand(0, 100),
                'short_description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }

        for ($i=0; $i < 5; $i++) { 
            DB::table('categories')->insert([
                'name' => str_random(10),
                'parent_id' => 2,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }

        for( $i = 0; $i < 20; $i++ ):
        DB::table('posts')->insert([
            'title' => str_random(30),
            'category_id' => mt_rand(11, 15),
            'short_content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat',
            'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        endfor;
        
    }
}
