<?php $__env->startSection('content'); ?>
<div id="sliderHome" class="carousel slide" data-ride="carousel">
	<div class="carousel-inner">
		<div class="carousel-item active">
			<img class="d-block w-100" src="<?php echo e(asset('uploads/sliders/slider-04.jpg')); ?>" alt="First slide">
		</div>
		<div class="carousel-item">
			<img class="d-block w-100" src="<?php echo e(asset('uploads/sliders/slider-06.jpg')); ?>" alt="Second slide">
		</div>
		<div class="carousel-item">
			<img class="d-block w-100" src="<?php echo e(asset('uploads/sliders/slider-02.jpg')); ?>" alt="Third slide">
		</div>
	</div>
	<a class="carousel-control-prev" href="#sliderHome" role="button" data-slide="prev">
		<span class="carousel-control-prev-icon" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>
	<a class="carousel-control-next" href="#sliderHome" role="button" data-slide="next">
		<span class="carousel-control-next-icon" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>
</div>
<style type="text/css">
	.carousel-item img {max-height: 550px;}
</style>
<div class="container">
	<div class="row favour">
		<div class="col-md-6 item-favour free-ship">
			<div class="wrapper">
				<span class="text">Free Ship Nội thành</span>
				<span class="icon-free-ship"><i class="fa fa-truck"></i></span>
			</div>
		</div>
		<div class="col-md-6 item-favour gift">
			<div class="wrapper">
				<span class="text">Quà tặng cuối tuần</span>
				<span class="icon-gift"><i class="fa fa-gift"></i></span>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
			<?php if(isset($total_search)): ?>
			<h3>Tìm thấy: <?php echo e($total_search); ?> kết quả</h3>
			<?php endif; ?>
			<?php if(isset($title_category)): ?>
			<h3><?php echo e($title_category); ?></h3>
			<?php endif; ?>
		</div>
		<div class="col-md-6">
			<div class="dropdown float-right">
			  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
			    Lọc sản phẩm
			  </button>
			  <div class="dropdown-menu">
			    <a class="dropdown-item" href="<?php echo e(route('productOrder', ['orderBy' => 'price', 'order' => 'asc'])); ?>">Theo giá từ thấp đến cao</a>
			    <a class="dropdown-item" href="<?php echo e(route('productOrder', ['orderBy' => 'price', 'order' => 'desc'])); ?>">Theo giá từ cao xuống thấp</a>
			    <a class="dropdown-item" href="<?php echo e(route('productOrder', ['orderBy' => 'sex', 'order' => '1'])); ?>">Theo giới tính: Nam</a>
			    <a class="dropdown-item" href="<?php echo e(route('productOrder', ['orderBy' => 'sex', 'order' => '0'])); ?>">Theo giới tính: Nữ</a>
			  </div>
			</div>
		</div>
	</div>
	<div class="row list-product">
		
		<?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<div class="col-md-3 product">
			<div class="card">
				<img class="card-img-top" src="<?php echo e(asset($product->image)); ?>" alt="Card image cap">
				<div class="card-body">
					<h5 class="card-title"><?php echo e($product->name); ?></h5>
					<p class="card-text">
						<?php if($product->sale != 0): ?>
						<span class="price-sale">
							<?php echo e(number_format(($product->price * $product->sale) / 100, 0, ',', '.')); ?>đ
						</span>
						<span class="price-primary">
							<del><?php echo e(number_format($product->price, 0, ',', '.')); ?>đ</del>
						</span>
						<?php endif; ?>
						<?php if($product->sale == 0): ?> 
							<span class="price-sale">
								<?php echo e(number_format($product->price, 0, ',', '.')); ?>đ
							</span>
						<?php endif; ?>
					</p>
					<a href="<?php echo e(route('showProduct', ['id' => $product->id])); ?>" class="btn btn-success">Chi tiết</a>
				</div>
			</div>
		</div>
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		
	</div>
	
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('client.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>