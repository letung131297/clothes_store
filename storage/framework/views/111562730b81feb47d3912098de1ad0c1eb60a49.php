<?php $__env->startSection('title'); ?>
Sản phẩm
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="container-fluid page-title">
	<div class="container">
		<p class="title">Chi tiết sản phẩm</p>
	</div>
</div>
<div class="container single-product">
	
	<div class="row">
		<div class="col-md-4 product-image">
			<div class="wrapper">
				<img src="<?php echo e(asset('uploads/product_images/600x600.png')); ?>">
			</div>
		</div>
		<div class="col-md-8 product-info">
			<h3 class="product-name"><?php echo e($product->name); ?></h3>
			<span class="product-code">TS<?php echo e($product->id); ?></span>
			<div class="price">
				<?php if($product->sale != 0): ?>
				<span class="current-price"><?php echo e(number_format( ( $product->price * (1- $product->sale/100) ), 0, ',', '.')); ?>đ</span>
				<span class="old-price"><del><?php echo e(number_format( $product->price, 0, ',', '.' )); ?></del>đ</span>
				<?php endif; ?>

				<?php if($product->sale == 0): ?>
				<span class="current-price"><?php echo e(number_format( $product->price, 0, ',', '.' )); ?>đ</span>
				<?php endif; ?>
			</div>
			<p class="short-description">
				<?php echo e($product->short_description); ?>

			</p>
			<p><i>Tồn kho: <?php echo e($product->quantity); ?></i></p>
			<div class="add-to-cart">
				<div class="wrapper">
					<form class="form-inline" action="<?php echo e(route('addToCart')); ?>" method="get">
						<div class="form-group">
							<input type="number" name="quantity" class="form-control" id="quantity" placeholder="Số lượng" value="1" max="<?php echo e($product->quantity); ?>" min="1">
							<input type="hidden" name="product_id" value="<?php echo e($product->id); ?>">
						</div>
						<button type="submit" class="btn btn-success btn-add-to-cart">Thêm giỏ hàng</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 description">
			<h3>Mô tả</h3>
			<div class="content">
				<?php echo e($product->description); ?>

			</div>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('client.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>