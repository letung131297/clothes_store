<?php $__env->startSection('title'); ?>
Giỏ hàng
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="container-fluid page-title">
	<div class="container">
		<p class="title">Giỏ hàng</p>
	</div>
</div>
<div class="container cart">
	<?php if(!empty($products)): ?>
	<form method="post" action="<?php echo e(route('updateCart')); ?>">
		<?php echo csrf_field(); ?>
	<div class="row">
		<table class="table table-inverse col-md-12 list-product">
			<thead>
				<tr>
					<th>MÃ SẢN PHẨM</th>
					<th>ẢNH SẢN PHẨM</th>
					<th>TÊN SẢN PHẨM</th>
					<th>GIÁ SẢN PHẨM</th>
					<th>SỐ LƯỢNG</th>
					<th>THÀNH TIỀN</th>
				</tr>
			</thead>
			<tbody>
				
				<?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<tr>
					<td>TS<?php echo e($product['id']); ?></td>
					<td>
						<a href="<?php echo e(route('showProduct', ['id' => $product['id']])); ?>">
							<img src="<?php echo e($product['image']); ?>" style="max-width: 100px;">
						</a>
					</td>
					<td>
						<a href="<?php echo e(route('showProduct', ['id' => $product['id']])); ?>">
							<?php echo e($product['name']); ?>

						</a>
					</td>
					<td><?php echo e(number_format($product['price'], 0, ',', '.')); ?>đ</td>
					<td><input type="number" name="cart[<?php echo e($product['id']); ?>]" value="<?php echo e($product['cart_quantity']); ?>"></td>
					<td><?php echo e(number_format($product['price'] * (1- $product['sale']/100) * $product['cart_quantity'], 0, ',', '.')); ?>đ</td>
				</tr>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</tbody>
		</table>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="float-left">
				<button type="submit" class="btn btn-success">Cập nhật giỏ hàng</button>
				<a href="<?php echo e(route('emptyCart')); ?>" class="btn btn-danger">Xóa giỏ hàng</a>
			</div>
			<div class="float-right text-right">
				<span>Tổng giá: </span><span class="total"><?php echo e(number_format($totalMoney, 0, ',', '.')); ?>đ</span>
				<div class=""><a href="<?php echo e(route('showCheckout')); ?>" class="btn btn-primary">Thanh toán</a></div>
			</div>
		</div>
	</div>
	</form>
	<?php endif; ?>
	<?php if(empty($products)): ?> <center><p>Giỏ hàng chưa có gì.</p></center> <?php endif; ?>

</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('client.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>