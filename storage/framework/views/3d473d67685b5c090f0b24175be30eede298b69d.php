<?php $__env->startSection('title'); ?>
Post - Edit
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<h1><center>Sửa bài viết</center></h1>
<div class="container">
	<form action="/admin/posts/update/<?php echo e($post->id); ?>" method="post">
		<div class="form-group row">
			<label for="" class="col-sm-2 form-control-label">Tiêu đề</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" placeholder="tên danh mục" name="title" value="<?php echo e($post->title); ?>">
			</div>
		</div>
		<div class="form-group row">
			<label for="" class="col-sm-2 form-control-label">Danh mục</label>
			<div class="col-sm-10">
				<select class="form-control" name="category_id">
					<option value="">--- Chọn ---</option>
					<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<option value="<?php echo e($cat->id); ?>" <?php if($cat->id == $post->category_id): ?> <?php echo e('selected'); ?>  <?php endif; ?> ><?php echo e($cat->name); ?></option>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</select>
			</div>
		</div>
		<div class="form-group row">
			<label for="" class="col-sm-2 form-control-label">Mô tả ngắn</label>
			<div class="col-sm-10">
				<textarea rows="5" class="form-control" name="short_content"><?php echo e($post->short_content); ?></textarea>
			</div>
		</div>
		<div class="form-group row">
			<label for="" class="col-sm-2 form-control-label">Nội dung</label>
			<div class="col-sm-10">
				<textarea rows="10" class="form-control" name="content"><?php echo e($post->content); ?></textarea>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-primary">Sửa</button>
			</div>
		</div>

		<?php echo csrf_field(); ?>
	</form>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>