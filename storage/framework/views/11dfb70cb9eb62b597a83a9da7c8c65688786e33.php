<?php $__env->startSection('title'); ?>
Category - List
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<h1><center>Danh sách</center></h1>
<div class="container">
    
    <?php if(session()->has('success')): ?>
        <div class="alert alert-success">
            <?php echo e(session()->get('success')); ?>

        </div>
    <?php endif; ?>

    <?php if(session()->has('error')): ?>
        <div class="alert alert-danger">
            <?php echo e(session()->get('error')); ?>

        </div>
    <?php endif; ?>

	<table id="categories_table" class="table table-striped table-bordered text-center" style="width:100%;">
        <thead>
            <tr>
                <th>ID</th>
                <th>Danh mục</th>
                <th>Danh mục cha</th>
                <th>Thời gian tạo</th>
                <th>Cập nhật lần cuối</th>
                <th>Thao tác</th>
            </tr>
        </thead>
        <tbody>
        	<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        	<tr>
                <td><?php echo e($cat->id); ?></td>
                <td><?php echo e($cat->name); ?></td>
                <td>
                	<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $innerCat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                	<?php if($cat->parent_id == $innerCat->id): ?>
                	<?php echo e($innerCat->name); ?>

                	<?php endif; ?>
                	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </td>
                <td><?php echo e($cat->created_at); ?></td>
                <td><?php echo e($cat->updated_at); ?></td>
                <td>
                    <?php if($cat->id > 2): ?>
                	<a href="<?php echo e(route('categories.show', ['id' => $cat->id])); ?>">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    </a>
                	<a href="<?php echo e(route('deleteCategory', ['id' => $cat->id])); ?>" class="confirm">
                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                    </a>
                    <?php endif; ?>
                </td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    	</tbody>
    	<tfoot>
            <tr>
                <th>ID</th>
                <th>Danh mục</th>
                <th>Danh mục cha</th>
                <th>Thời gian tạo</th>
                <th>Cập nhật lần cuối</th>
                <th>Thao tác</th>
            </tr>
        </tfoot>
	</table>
	<script type="text/javascript">
		$(document).ready( function () {
		    $('#categories_table').DataTable();
		} );
	</script>
</div>

<br>
<hr>
<br>

<h1><center>Thêm danh mục</center></h1>
<div class="container">
	<form action="/admin/categories/insert" method="post">
		<div class="form-group row">
			<label for="inputEmail3" class="col-sm-2 form-control-label">Tên danh mục</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="inputEmail3" placeholder="tên danh mục" name="name">
			</div>
		</div>
		<div class="form-group row">
			<label for="inputPassword3" class="col-sm-2 form-control-label">Danh mục cha</label>
			<div class="col-sm-10">
				<select class="form-control" name="parent_id">
					<option value="0">--- Chọn ---</option>
					<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<option value="<?php echo e($cat->id); ?>"><?php echo e($cat->name); ?></option>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</select>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-primary">Thêm</button>
			</div>
		</div>

		<?php echo csrf_field(); ?>
	</form>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>