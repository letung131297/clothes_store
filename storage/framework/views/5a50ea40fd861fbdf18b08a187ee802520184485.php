<?php $__env->startSection('title'); ?>
Sản phẩm
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="container-fluid page-title">
    <div class="container">
        <p class="title"><?php echo e($title_category); ?></p>
    </div>
</div>
<div class="container list-post">
    <div class="row">
        <div class="col-md-12">
            <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <article>
                <h3><?php echo e($post->title); ?></h3>
                <p class="short-description">
                    <?php echo e($post->short_content); ?>

                </p>
                <a href="<?php echo e(route('showSinglePost', ['id' => $post->id])); ?>">Xem tiếp</a>
            </article>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
    
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('client.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>