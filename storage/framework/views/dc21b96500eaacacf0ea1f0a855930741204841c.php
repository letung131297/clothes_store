<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href=" <?php echo e(route('adminDashboard')); ?> ">Clothes Store</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
  <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href=" <?php echo e(route('categories.index')); ?> ">Danh mục</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href=" <?php echo e(route('products.index')); ?> ">Sản phẩm</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href=" <?php echo e(route('orders.index')); ?> ">Đơn hàng</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href=" <?php echo e(route('posts.index')); ?> ">Tin tức</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href=" <?php echo e(route('indexStatistics')); ?> ">Thống kê</a>
      </li>
      <li class="nav-item dropdown">
            <?php if(auth()->guard()->check()): ?>
              <a class="nav-link dropdown-toggle username" href="#" id="logged" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Xin chào <?php echo e(\Auth::user()->name); ?>

              </a>
              <div class="dropdown-menu" aria-labelledby="logged">
                <a class="dropdown-item" href="<?php echo e(route('logout')); ?>"
                                 onclick="event.preventDefault();
                                               document.getElementById('logout-form').submit();">
                                  <?php echo e(__('Đăng xuất')); ?>

                              </a>

                              <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                  <?php echo csrf_field(); ?>
                              </form>
              </div>
              
                    <?php else: ?>
            <a class="nav-link dropdown-toggle" href="#" id="login-register" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Đăng nhập/Đăng ký
            </a>
            <div class="dropdown-menu" aria-labelledby="login-register">
              <a class="dropdown-item" href="<?php echo e(route('login')); ?>">Đăng nhập</a>
              <a class="dropdown-item" href="<?php echo e(route('register')); ?>">Đăng ký</a>
            </div>
                    <?php endif; ?>
          </li>
    </ul>
  </div>
</nav>