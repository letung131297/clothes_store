<?php $__env->startSection('title'); ?>
Tin tức
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="container-fluid page-title">
	<div class="container">
		<p class="title"><?php echo e($post->title); ?></p>
	</div>
</div>
<div class="container single-post">
	
	<div class="row">
		<div class="col-md-12">
			<div class="content">
				<?php echo e($post->content); ?>

			</div>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('client.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>