<!DOCTYPE html>
<html>
	<head>
		<title>Tĩn Store - <?php echo $__env->yieldContent('title'); ?></title>
		<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/app.css')); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/custom.css')); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo e(asset('font-awesome-4.7.0/css/font-awesome.min.css')); ?>">
		<script src="<?php echo e(asset('js/app.js')); ?>"></script>
		<script src="<?php echo e(asset('js/custom.js')); ?>"></script>
	</head>
	<body>
		<nav class="navbar navbar-expand-lg menu-client">
			<a class="navbar-brand" href="/">Tin Store</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item">
						<a class="nav-link" href="/">Home</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Giới thiệu</a>
					</li>
					
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Sản phẩm
						</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<?php if(count($category_menu[0]) != 0): ?>
								<?php $__currentLoopData = $category_menu[0]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category_sanpham): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<a class="dropdown-item" href="<?php echo e(route('showListProduct', ['category_id' => $category_sanpham->id])); ?>"><?php echo e($category_sanpham->name); ?></a>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							<?php endif; ?>
						</div>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Tin tức
						</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<?php if(count($category_menu[1]) != 0): ?>
								<?php $__currentLoopData = $category_menu[1]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category_tintuc): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<a class="dropdown-item" href="<?php echo e(route('showListPost', ['category_id' => $category_tintuc->id])); ?>"><?php echo e($category_tintuc->name); ?></a>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							<?php endif; ?>
						</div>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo e(route('showCart')); ?>">Giỏ hàng</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Liên hệ</a>
					</li>
					<li class="nav-item dropdown">
						<?php if(auth()->guard()->check()): ?>
							<a class="nav-link dropdown-toggle username" href="#" id="logged" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Xin chào <?php echo e(\Auth::user()->name); ?>

							</a>
							<div class="dropdown-menu" aria-labelledby="logged">
								<a class="dropdown-item" href="<?php echo e(route('logout')); ?>"
	                               onclick="event.preventDefault();
	                                             document.getElementById('logout-form').submit();">
	                                <?php echo e(__('Đăng xuất')); ?>

	                            </a>

	                            <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
	                                <?php echo csrf_field(); ?>
	                            </form>
							</div>
							
		                <?php else: ?>
						<a class="nav-link dropdown-toggle" href="#" id="login-register" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Đăng nhập/Đăng ký
						</a>
						<div class="dropdown-menu" aria-labelledby="login-register">
							<a class="dropdown-item" href="<?php echo e(route('login')); ?>">Đăng nhập</a>
							<a class="dropdown-item" href="<?php echo e(route('register')); ?>">Đăng ký</a>
						</div>
		                <?php endif; ?>
					</li>
				</ul>
				<form class="form-inline my-2 my-lg-0" action="<?php echo e(route('searchProduct')); ?>" method="get">
					<input class="form-control mr-sm-2" type="search" placeholder="Tìm kiếm" name="s" aria-label="Search">
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Tìm kiếm</button>
				</form>

			</div>
		</nav>